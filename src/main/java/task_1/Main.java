package task_1;

import task_1.FunctionalInterface;

public class Main {
    public static void main(String[] args) {


        FunctionalInterface lambda = (a, b, c) -> (a+b+c);
        FunctionalInterface lambda1 = (a, b, c) -> { if ( a > b && a > c) return a;
                                                        else if (b > c) return b;
                                                        else return c;
                                                     };
        FunctionalInterface lambda2 = (a, b, c) -> (a+b+c)/3;


        System.out.println("sum = " +lambda.returnInt(5,6,8));
        System.out.println("max = " +lambda1.returnInt(5,6,8));
        System.out.println("average = " +lambda2.returnInt(5,6,8));

    }
}

