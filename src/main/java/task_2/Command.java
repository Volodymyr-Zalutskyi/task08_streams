package task_2;

public interface Command {
    public void perform();
}
