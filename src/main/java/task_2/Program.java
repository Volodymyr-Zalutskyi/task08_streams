package task_2;


public class Program {

    public static void main(String[] args) {

        final Menu macro = new Menu();
        final Editor editor = new MuneEditor();

        macro.record(() -> new Open(editor));
        macro.record(() -> new Save(editor));

        macro.run(); // 1st way

        macro.record(() -> editor.open());
        macro.record(() -> editor.save());

        macro.run(); // 2nd way

        macro.record(editor::open);
        macro.record(editor::save);

        macro.run(); // 3rd way
    }

}
