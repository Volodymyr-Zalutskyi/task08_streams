package task_2;

import java.util.ArrayList;
import java.util.List;

 class MuneEditor implements Editor{

    private final List<String> actions = new ArrayList();


    @Override
    public void open() {
        this.actions.add("open");
    }

    @Override
    public void save() {
        this.actions.add("save");
    }
}
