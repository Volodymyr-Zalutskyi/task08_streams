package task_2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Menu {

    private final List<Command> commands;

    public Menu() {
        this.commands = new ArrayList();
    }

    public void record(Command command) {
        this.commands.add(command);
    }

    public void run() {
        this.commands.forEach(Command::perform);
    }


}
