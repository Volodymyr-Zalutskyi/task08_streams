package task_2;

public class Open implements Command {
    private final Editor editor;

    public Open(final Editor editor) {
        super();
        this.editor = editor;
    }

    @Override
    public void perform() {
        this.editor.open();
    }
}
