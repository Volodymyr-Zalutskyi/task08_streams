package task_2;

public interface Editor {
    public void open();
    public void save();
}
