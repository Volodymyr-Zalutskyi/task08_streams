package task_2;

public class Save implements Command {
    private final Editor editor;

    public Save(final Editor editor) {
        super();
        this.editor = editor;
    }

    @Override
    public void perform() {
        this.editor.save();
    }
}
