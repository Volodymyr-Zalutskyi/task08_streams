package task_3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {


        List<Integer> i = returnInteger ();
        i.forEach(x -> System.out.print(x + " "));
        int x = i.stream().reduce(0, (a, b) -> a+b);
        System.out.println("sum-reduce = " + x);

        IntSummaryStatistics statistic = i.stream().collect(Collectors.summarizingInt(p ->p.intValue()));
        System.out.println(statistic);

        int max = Collections.max(i);
        System.out.println("max = " + max);

        Optional<Integer> sum = i.stream().reduce((a, b) -> a+b);
        System.out.println("sum = " + sum);
        int sum1 = i.stream().mapToInt(a ->a).sum();
        System.out.println("sum1 = " + sum1);

        double average = i.stream().mapToDouble(a -> a).average().orElse(0);
        System.out.println("average = " + average);


        List <Integer> bigThanAverage = i.stream().filter(n -> n > average).collect(Collectors.toList());
        System.out.println("values that are bigger than average = " + bigThanAverage);


    }

    private static List<Integer> returnInteger () {
        Random r = new Random ();
        int size = r.nextInt(10)+1;
        List<Integer> randomList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int number = r.nextInt(10) + 1;
             randomList.add(number);
        }
        return randomList;
    }

    private static void streamInteger () {
        Random r = new Random ();
        Stream.generate(() -> r.nextInt(10) + 1)
                .limit(7).forEach(System.out::print);
    }

    private static void integerStream () {
        IntStream stream = IntStream.generate(()
                -> (int) (Math.random() * 10));
        stream.limit(7).forEach(System.out::print);
    }


}
