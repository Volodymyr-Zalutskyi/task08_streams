package task_4;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String line;
        String x = "";
        System.out.println("Please insert text:");
        while (!(line = sc.nextLine()).equals("")) {
            x += (line + ",");
        }
        String[] integersAsText = x.split(",");
        System.out.println(Arrays.toString(integersAsText));

        Stream<String> numberText = Arrays.stream(integersAsText);
        List<String> listNumberText = numberText.map(String::new).distinct().collect(Collectors.toList());
        System.out.println("Unique array -" +listNumberText);

        //print number of unique words
        Arrays.stream(integersAsText)
                .distinct()
                .collect(Collectors.toList())
                .forEach(n -> System.out.print(n + " "));

        System.out.println("");

        List<String> listNumberSorted = listNumberText.stream().sorted().collect(Collectors.toList());
        System.out.println("Sorted -" + listNumberSorted);

        List<String> listNumberUpperCase = listNumberSorted.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println("Upper Case -" +listNumberUpperCase);

        for (String number : listNumberText) {
            System.out.print(number + " - ");
            long numberOfEach = Arrays.stream(integersAsText).filter(n -> n.equals(number)).count();
            System.out.println(numberOfEach);
        }





    }
}
